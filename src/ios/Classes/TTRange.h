//
//  TTRange.h
//  HelloCordova
//
//  Created by Thien on 5/17/16.
//
//

#import <Foundation/Foundation.h>

@interface TTRange : NSObject

@property (nonatomic, strong) NSNumber *min; // int
@property (nonatomic, strong) NSNumber *max; // int

@end
